﻿#include <iostream>
#include <time.h>



int main()
{
    const int N = 5;
    int massive[N][N];
    for (int i = 0; i < N; ++i) 
    {
        for (int j = 0; j < N; ++j) 
        {
            massive[i][j] =i+j;
            std::cout << massive[i][j];

        }
        std::cout << "\n";
    }

    struct tm buf;
    time_t t = time(0);
    localtime_s(&buf, &t);
    int day = buf.tm_mday;

    int sum = 0;
    for (int j = 0; j < N; ++j) {
        sum += massive[day % N][j];
    }
    std::cout << "Sum of massive with index " << day % N << ": " << sum << '\n';

    return 0;
}